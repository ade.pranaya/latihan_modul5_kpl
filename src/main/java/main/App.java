package main;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import spark.utils.IOUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.zip.CRC32;

import static spark.Spark.get;
import static spark.Spark.post;

public class App {
    public static void main(String[] args) {
        //konfigurasi Velocity Template Engine
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new
                VelocityTemplateEngine(configuredEngine);

        //Routes
        get("/crc", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String plain = req.queryParamOrDefault("plain", "");

            String cipherResult = null;
            //membuat objek cipher
            Cipher cipher = null;
            cipher = Cipher.getInstance("AES");

            //membuat keygenerator
            KeyGenerator keyGenerator =
                    KeyGenerator.getInstance("AES"); //Advance Encription Standard

            //membuat kunci keamanan acak
            int keyBitSize = 256;
            SecureRandom secureRandom = new SecureRandom();
            keyGenerator.init(keyBitSize, secureRandom);

            //membuat kunci keamanan
            SecretKey secretKey = keyGenerator.generateKey();

            //inisialisasi cipher
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            byte[] cipherText = cipher.doFinal(plain.getBytes());
            cipherResult = new String(cipherText);

            model.put("crc", cipherResult);
            String templatePath = "/views/crc.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        get("/crcFile", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String templatePath = "/views/crc_file.vm";
            String crc = req.queryParamOrDefault("crc", "");
            model.put("crc", crc);
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });
        post("/crcFile", (req, res) -> {
            long crcResult = 0;
            String cipherResult = null;
            try {
                req.attribute("org.eclipse.jetty.multipartConfig", new
                        MultipartConfigElement(System.getProperty("user.dir") + "//tmp"));
                Part filePart = req.raw().getPart("myfile");
                if (filePart != null) {
                    try (InputStream inputStream =
                                 filePart.getInputStream()) {

                        //membuat objek cipher
                        Cipher cipher = null;
                        cipher = Cipher.getInstance("AES");

                        //membuat keygenerator
                        KeyGenerator keyGenerator =
                                KeyGenerator.getInstance("AES"); //Advance Encription Standard

                        //membuat kunci keamanan acak
                        int keyBitSize = 256;
                        SecureRandom secureRandom = new SecureRandom();
                        keyGenerator.init(keyBitSize, secureRandom);

                        //membuat kunci keamanan
                        SecretKey secretKey = keyGenerator.generateKey();

                        //inisialisasi cipher
                        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

                        byte[] cipherText = cipher.doFinal(IOUtils.toByteArray(inputStream));
                        cipherResult = new String(cipherText);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            res.redirect("/crcFile?crc=" + cipherResult);
            return res;
        });
    }
}